/*

Copyright (c) 2020, CIMCON Software India Pvt. Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef FILE_C
#define FILE_C

#include "Defines.hpp"
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

extern bool file_is_exists(const char* filename);
extern char* file_get_parent_directory(const char* filename);
extern char* file_get_basename(const char* filename);
extern char* file_get_extension(const char* filename);

#ifdef __cplusplus
}
#endif

#endif
